import people_data from '../data/people_data.json';
import { GetAllParams } from '../types/types';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll({ pageNumber, pageSize }: GetAllParams) {
        const startIndex = (pageNumber - 1) * pageSize;
        const endIndex = pageNumber * pageSize;
        const finalList = people_data.slice(startIndex, endIndex);
        console.log(finalList, finalList.length);
        return finalList;
    }
}
