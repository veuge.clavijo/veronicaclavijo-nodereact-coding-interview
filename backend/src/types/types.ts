export interface GetAllParams {
    pageSize: number;
    pageNumber: number;
}