import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [filter, setFilter] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(true);
  const [pagination, setPagination] = useState<number>(1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await backendClient.getAllUsers({
          pageNumber: pagination,
          pageSize: 10,
        });
        setUsers(result.data);
        setLoading(false);
      } catch (error) {
        console.log({ error });
        setLoading(false);
      }
    };

    fetchData();
  }, [pagination]);

  const updateFilter = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilter(event?.target.value)
  }
  const setPage = (dir: "+" | "-") => {
    setPagination(dir === "+" ? pagination + 1 : pagination - 1);
  }

  useEffect(() => {
    // const newUsers = users.filter(u => u.gender.toLowerCase().includes(filter.toLowerCase()) || u?.title?.toLowerCase().includes(filter.toLowerCase()) || u?.company?.toLowerCase().includes(filter.toLowerCase()))
    const newUsers = users.filter(u => u.email.toLowerCase().includes(filter.toLowerCase()))
    setUsers(newUsers);
  }, [filter]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <button onClick={() => setPage("-")}>{"<<"}</button>
            <button onClick={() => setPage("+")}>{">>"}</button>
            <input type={"text"} value={filter} onChange={updateFilter} />
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
