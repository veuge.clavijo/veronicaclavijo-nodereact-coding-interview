import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

interface Pagination {
  pageNumber: number;
  pageSize: number;
}

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers({ pageNumber, pageSize }: Pagination): Promise<{ data: IUserProps[] }> {
    return (await axios.get(`${this.baseUrl}/people/all?pageNumber=${pageNumber}&pageSize=${pageSize}`, {})).data;
  }
}
